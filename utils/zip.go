package vimmgrabber

import (
	"archive/zip"
	"io"
	"log"
	"os"
	"path/filepath"
	"strings"

	unarr "github.com/gen2brain/go-unarr"
)

// ContainsFileExtension checks if the filename has file extensions
func (t *VimmGrabber) ContainsFileExtension(fileExtensions []string, fileName string) bool {
	fileExtension := filepath.Ext(fileName)

	for _, extension := range fileExtensions {
		if extension == fileExtension {
			return true
		}
	}

	return false
}

// Extract7zFile extracts 7z files
func (t *VimmGrabber) Extract7zFile(zipFileSavePath string, directory string) ([]string, error) {
	zipReader, err := unarr.NewArchive(zipFileSavePath)
	var foundFileNames []string

	if err != nil {
		panic(err)
	}

	for {
		err := zipReader.Entry()

		if err != nil {
			if err == io.EOF {
				break
			} else {
				panic(err)
			}
		}

		fileName := zipReader.Name()
		containsDirectory := strings.Contains(fileName, "/")
		var fullGameDirectoryPath string

		if containsDirectory {
			gameDirectory := strings.Split(fileName, "/")[0]
			_, err := os.Stat(gameDirectory)

			if os.IsNotExist(err) {
				fullGameDirectoryPath = filepath.Join(directory, gameDirectory)

				os.Mkdir(fullGameDirectoryPath, 0777)
			}
		} else {
			fullGameDirectoryPath = directory
		}

		if t.ContainsFileExtension(CueFileExtensions, fileName) {
			fullFilePath := filepath.Join(directory, fileName)
			toFile, err := os.Create(fullFilePath)

			if err != nil {
				panic(err)
			}

			cueBytes, err := zipReader.ReadAll()

			if err != nil {
				panic(err)
			}

			_, err = toFile.WriteString(string(cueBytes))

			if err != nil {
				return nil, err
			}

			toFile.Close()

			foundFileNames = append(foundFileNames, fileName)
		} else if t.ContainsFileExtension(GameFileExtensions, fileName) {
			fullFilePath := filepath.Join(directory, fileName)
			toFile, err := os.Create(fullFilePath)

			if err != nil {
				panic(err)
			}

			_, err = io.Copy(toFile, zipReader)

			if err != nil {
				return nil, err
			}

			toFile.Close()

			foundFileNames = append(foundFileNames, fileName)
		} else {
			log.Println("no file extensions match for " + fileName)
		}
	}

	zipReader.Close()

	return foundFileNames, nil
}

// ExtractZipFile extracts zip files
func (t *VimmGrabber) ExtractZipFile(zipFileSavePath string, directory string) ([]string, error) {
	zipReader, err := zip.OpenReader(zipFileSavePath)
	var foundFileNames []string

	if err != nil {
		return nil, err
	}

	for _, file := range zipReader.File {
		fileName := file.Name

		if t.ContainsFileExtension(GameFileExtensions, fileName) {
			newFilePath := filepath.Join(directory, fileName)
			fromFile, err := file.Open()

			if err != nil {
				return nil, err
			}

			toFile, err := os.Create(newFilePath)

			if err != nil {
				return nil, err
			}

			_, err = io.Copy(toFile, fromFile)

			if err != nil {
				return nil, err
			}

			foundFileNames = append(foundFileNames, fileName)

			fromFile.Close()
			toFile.Close()
		} else {
			log.Println("no file extensions match for " + fileName)
		}
	}

	zipReader.Close()

	return foundFileNames, nil
}
