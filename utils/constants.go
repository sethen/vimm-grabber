package vimmgrabber

// CueFileExtensions game file extensions for games
var CueFileExtensions = []string{
	".cue",
}

// GameFileExtensions game file extensions for games
var GameFileExtensions = []string{
	".bin",
	".ccd",
	".ciso",
	".cue",
	".gb",
	".gba",
	".gbc",
	".gcm",
	".gen",
	".img",
	".iso",
	".md",
	".mdf",
	".mds",
	".nes",
	".nds",
	".sfc",
	".smd",
	".sub",
	".wbfs",
	".z64",
}
