## Vimm Grabber

Vimm Grabber is a piece of software that grabs ROMs from vimm.net in sequential order.

### Installation

To use this, you need to have `go` installed on your system with the command line tools available.

Once you've pulled the project, you can use `go build && go install` to install the `vimm-grabber` command line utility.  You can run the `vimm-grabber` command line utility with any of the command line options listed below.

You _must_ include the games you want to download.  For instance, if you want just `gameboy` games, you need to run `vimm-grabber -gameboy`.  You can include multiple flags depending on what games you want.  Vimm Grabber will not run without having games defined for downloading.

Vimm Grabber will automatically start creating directories for games if they don't exist and download games to those directories.  So, it's best to run this in an isolated area where directories can be created.

### Command Line Options

```
Usage of vimm-grabber:
	-from-game-id int
		game id to start scraping (default 1)
	-gameboy
		include gameboy games
	-gameboy-advance
		include gameboy advance games
	-gameboy-color
		include gameboy color games
	-gamecube
		include gamecube games
	-genesis
		include genesis games
	-nes
		include nes games
	-nintendo-64
		include nintendo 64 games
	-nintendo-ds
		include nintendo ds games
	-ps1
		include ps1 games
	-ps2
		include ps2 games
	-snes
		include snes games
	-throttle-seconds int
		throttle between requests (default 5)
	-unzip
		unzip to directory (default true)
	-wii
		include wii games
```
