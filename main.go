package main

import (
	"errors"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"github.com/fatih/color"
	vimmgrabber "gitlab.com/sethen/vimm-grabber/utils"
)

func main() {
	vimmGrabber := new(vimmgrabber.VimmGrabber)

	flag.BoolVar(&vimmGrabber.Debug, "debug", false, "show debug logs while running")
	flag.BoolVar(&vimmGrabber.Dreamcast, "dreamcast", false, "include dreamcast games")
	flag.IntVar(&vimmGrabber.FromGameID, "from-game-id", 1, "game id to start scraping")
	flag.BoolVar(&vimmGrabber.GameBoy, "gameboy", false, "include gameboy games")
	flag.BoolVar(&vimmGrabber.GameBoyAdvance, "gameboy-advance", false, "include gameboy advance games")
	flag.BoolVar(&vimmGrabber.GameBoyColor, "gameboy-color", false, "include gameboy color games")
	flag.BoolVar(&vimmGrabber.GameCube, "gamecube", false, "include gamecube games")
	flag.BoolVar(&vimmGrabber.Genesis, "genesis", false, "include genesis games")
	flag.BoolVar(&vimmGrabber.Nintendo, "nintendo", false, "include nintendo games")
	flag.BoolVar(&vimmGrabber.Nintendo64, "nintendo-64", false, "include nintendo 64 games")
	flag.BoolVar(&vimmGrabber.NintendoDs, "nintendo-ds", false, "include nintendo ds games")
	flag.BoolVar(&vimmGrabber.PlayStation, "playstation", false, "include playstation games")
	flag.BoolVar(&vimmGrabber.PlayStation2, "playstation-2", false, "include playstation 2 games")
	flag.IntVar(&vimmGrabber.ThrottleSeconds, "throttle-seconds", 5, "throttle between requests")
	flag.BoolVar(&vimmGrabber.SuperNintendo, "super-nintendo", false, "include super nintendo games")
	flag.IntVar(&vimmGrabber.ToGameID, "to-game-id", 20122, "game id to stop scraping")
	flag.BoolVar(&vimmGrabber.Unzip, "unzip", true, "unzip to directory")
	flag.BoolVar(&vimmGrabber.Wii, "wii", false, "include wii games")

	flag.Parse()

	fmt.Println("welcome to the vimm.net scraper, please use responsibly!")
	fmt.Println("")

	if vimmGrabber.ThrottleSeconds < 5 {
		vimmGrabber.ThrottleSeconds = 5
	}

	if !vimmGrabber.Dreamcast && !vimmGrabber.GameBoy && !vimmGrabber.GameBoyAdvance && !vimmGrabber.GameBoyColor && !vimmGrabber.GameCube && !vimmGrabber.Genesis && !vimmGrabber.Nintendo && !vimmGrabber.Nintendo64 && !vimmGrabber.NintendoDs && !vimmGrabber.PlayStation && !vimmGrabber.PlayStation2 && !vimmGrabber.SuperNintendo && !vimmGrabber.Wii {
		fmt.Println(color.RedString("✗"), "no games have been included to download, please pass games to download")

		os.Exit(1)
	}

	logFile, err := os.OpenFile("log", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)

	if err != nil {
		panic(err)
	}

	defer logFile.Close()

	log.SetOutput(logFile)

	if vimmGrabber.Debug {
		fmt.Println(color.YellowString("!"), "starting downloading from game id", vimmGrabber.FromGameID, "and stopping at game id", vimmGrabber.ToGameID)
	}

	for i := vimmGrabber.FromGameID; i <= vimmGrabber.ToGameID; i++ {
		if vimmGrabber.Debug {
			fmt.Println(color.YellowString("!"), "gathering game details for game at id", i)
		}

		gameID := strconv.Itoa(i)
		gameDetailsResponse, err := vimmGrabber.GatherGameDetailsAtGameID(gameID)

		if err != nil {
			if vimmGrabber.Debug {
				fmt.Println(color.RedString("✗"), "couldn't gather game details for game at id", gameID+", retrying")
				fmt.Println(color.GreenString("✓"), "throttling for", vimmGrabber.ThrottleSeconds, "seconds before next request")
				fmt.Println("")
			}

			time.Sleep(time.Duration(vimmGrabber.ThrottleSeconds) * time.Second)

			i--

			continue
		}

		if gameDetailsResponse.StatusCode == http.StatusNotFound {
			if vimmGrabber.Debug {
				fmt.Println(color.RedString("✗"), "game at id", gameID, "does not exist")
				fmt.Println("")
			}

			continue
		} else if gameDetailsResponse.StatusCode != http.StatusOK {
			if vimmGrabber.Debug {
				fmt.Println(color.RedString("✗"), gameID, "isn't returning details, retrying")
				fmt.Println(color.GreenString("✓"), "throttling for", vimmGrabber.ThrottleSeconds, "seconds before next request")
				fmt.Println("")
			}

			time.Sleep(time.Duration(vimmGrabber.ThrottleSeconds) * time.Second)

			i--

			continue
		}

		if vimmGrabber.Debug {
			fmt.Println(color.GreenString("✓"), "response code for game details was", gameDetailsResponse.Status)
		}

		body, err := ioutil.ReadAll(gameDetailsResponse.Body)

		if err != nil {
			fmt.Println(err)
		}

		webpage := fmt.Sprintf("%s", body)

		if strings.Contains(webpage, "Error: Game not found.") {
			if vimmGrabber.Debug {
				fmt.Println(color.RedString("✗"), "game at id", gameID, "does not exist")
				fmt.Println("")
			}

			continue
		}

		if vimmGrabber.Debug {
			fmt.Println(color.YellowString("!"), "determining game type")
		}

		directory, err := vimmGrabber.DetermineGameType(webpage)

		if err != nil {
			if vimmGrabber.Debug {
				fmt.Println(color.RedString("✗"), err)
				fmt.Println("")
			}

			continue
		}

		if vimmGrabber.Debug {
			fmt.Println(color.GreenString("✓"), "determined game type as", directory)
			fmt.Println(color.YellowString("!"), "determining number of discs")
		}

		numberOfDiscs := vimmGrabber.DetermineNumberOfDiscs(webpage)

		if vimmGrabber.Debug {
			fmt.Println(color.GreenString("✓"), "found", numberOfDiscs, "number of discs to download")
		}

		gameDetailsResponse.Body.Close()

		_, err = os.Stat(directory)

		if os.IsNotExist(err) {
			if vimmGrabber.Debug {
				fmt.Println(color.YellowString("!"), "creating directory", directory, "since none found")
			}

			err := os.Mkdir(directory, 0777)

			if err != nil {
				panic(err)
			}

			if vimmGrabber.Debug {
				fmt.Println(color.GreenString("✓"), "created directory", directory)
			}
		}

		for j := 1; j < numberOfDiscs+1; j++ {
			var downloadResponse *http.Response
			var err error

			if j == 1 {
				if vimmGrabber.Debug {
					fmt.Println(color.YellowString("!"), "starting download for game with id", gameID)
				}

				downloadResponse, err = vimmGrabber.DownloadGameAtGameID(gameID)
			} else {
				discNumber := strconv.Itoa(j)

				if vimmGrabber.Debug {
					fmt.Println(color.YellowString("!"), "starting download for game with id", gameID, "and disc number", discNumber)
				}

				downloadResponse, err = vimmGrabber.DownloadGameDiscAtGameID(gameID, discNumber)
			}

			if err != nil {
				if vimmGrabber.Debug {
					fmt.Println(color.RedString("✗"), "there has been an error downloading the game at id", gameID+", retrying")
					fmt.Println(color.GreenString("✓"), "throttling for", vimmGrabber.ThrottleSeconds, "seconds before next request")
					fmt.Println("")
				}

				time.Sleep(time.Duration(vimmGrabber.ThrottleSeconds) * time.Second)

				i--

				continue
			}

			if downloadResponse.StatusCode == http.StatusNotFound {
				if vimmGrabber.Debug {
					fmt.Println(color.RedString("✗"), "game at id", gameID, "does not exist")
					fmt.Println("")
				}

				continue
			} else if downloadResponse.StatusCode != http.StatusOK {
				if vimmGrabber.Debug {
					fmt.Println(color.RedString("✗"), "game at id", gameID, "isn't starting download, retrying")
					fmt.Println(color.GreenString("✓"), "throttling for", vimmGrabber.ThrottleSeconds, "seconds before next request")
					fmt.Println("")
				}

				time.Sleep(time.Duration(vimmGrabber.ThrottleSeconds) * time.Second)

				i--

				continue
			}

				if vimmGrabber.Debug {
					fmt.Println(color.GreenString("✓"), "response code for game download was", gameDetailsResponse.Status)
				}

			responseHeader := downloadResponse.Header

			contentType, contentTypeExists := responseHeader["Content-Type"]
			contentDisposition, contentDispositionExists := responseHeader["Content-Disposition"]

			if !contentTypeExists || !contentDispositionExists {
				if vimmGrabber.Debug {
					fmt.Println(color.RedString("✗"), "game at id", gameID, "is missing vital headers, retrying")
					fmt.Println(color.GreenString("✓"), "throttling for", vimmGrabber.ThrottleSeconds, "seconds before next request")
					fmt.Println("")
				}

				time.Sleep(time.Duration(vimmGrabber.ThrottleSeconds) * time.Second)

				i--

				continue
			}

			contentTypeFirstValue := contentType[0]
			attachmentHeaderFirstValue := contentDisposition[0]
			firstQuotationIndex := strings.Index(attachmentHeaderFirstValue, "\"")
			attachmentHeaderSubstr := attachmentHeaderFirstValue[firstQuotationIndex + 1:]
			nextQuotationIndex := strings.Index(attachmentHeaderSubstr, "\"")
			zipFileName := attachmentHeaderSubstr[:nextQuotationIndex]
			zipFileSavePath := filepath.Join(directory, zipFileName)

			if vimmGrabber.Debug {
				fmt.Println(color.YellowString("!"), "downloading", zipFileName, "to", zipFileSavePath)
			}

			zipFile, err := os.Create(zipFileSavePath)

			if err != nil {
				panic(err)
			}

			_, err = io.Copy(zipFile, downloadResponse.Body)

			if err != nil {
				if vimmGrabber.Debug {
					fmt.Println(color.RedString("✗"), "there has been an error downloading the game at id", gameID+", retrying")
					fmt.Println(color.GreenString("✓"), "throttling for", vimmGrabber.ThrottleSeconds, "seconds before next request")
					fmt.Println("")
				}

				time.Sleep(time.Duration(vimmGrabber.ThrottleSeconds) * time.Second)

				i--

				continue
			}

			zipFile.Close()
			downloadResponse.Body.Close()

			if vimmGrabber.Debug {
				fmt.Println(color.GreenString("✓"), "downloaded", zipFileName, "to", zipFileSavePath)
			}

			if vimmGrabber.Unzip {
				if vimmGrabber.Debug {
					fmt.Println(color.YellowString("!"), "extracting", zipFileSavePath)
				}

				var fileNames []string

				if contentTypeFirstValue == "application/zip" || contentTypeFirstValue == "application/x-zip-compressed" {
					if vimmGrabber.Debug {
						fmt.Println(color.YellowString("!"), "found application/zip or application/x-zip-compressed content type")
					}

					fileNames, err = vimmGrabber.ExtractZipFile(zipFileSavePath, directory)
				} else if contentTypeFirstValue == "application/x-7z-compressed" {
					if vimmGrabber.Debug {
						fmt.Println(color.YellowString("!"), "found application/z-7z-compressed content type")
					}

					fileNames, err = vimmGrabber.Extract7zFile(zipFileSavePath, directory)
				} else {
					errStr := errors.New("could not find content type")

					panic(errStr)
				}

				if err != nil {
					log.Println(err)
				}

				if vimmGrabber.Debug {
					fmt.Println(color.GreenString("✓"), "extracted", zipFileSavePath)

					for _, fileName := range fileNames {
						fmt.Println(color.GreenString("✓"), "received", fileName)
					}

					fmt.Println(color.YellowString("!"), "removing", zipFileSavePath)
				}

				os.RemoveAll(zipFileSavePath)

				if vimmGrabber.Debug {
					fmt.Println(color.GreenString("✓"), "removed", zipFileSavePath)
				}
			} else if vimmGrabber.Debug {
				fmt.Println(color.GreenString("✓"), "received", zipFileSavePath+"!")
			}

			if vimmGrabber.Debug {
				log.Println("finished downloading game at game id", gameID)
				fmt.Println(color.GreenString("✓"), "throttling for", vimmGrabber.ThrottleSeconds, "seconds before next request")
				fmt.Println("")
			}

			time.Sleep(time.Duration(vimmGrabber.ThrottleSeconds) * time.Second)
		}
	}

	fmt.Println("All done!")
}
