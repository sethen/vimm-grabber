package vimmgrabber

import (
	"errors"
	"fmt"
	"strings"
)

func addHTMLPrefix(console string) (string) {
	return "sectionTitle\">" + console
}

// DetermineGameType determines the game type
func (t *VimmGrabber) DetermineGameType(webpage string) (string, error) {
	dreamcastTitle := "Dreamcast"
	gameBoyTitle := "Game Boy"
	gameBoyAdvanceTitle := "Game Boy Advance"
	gameBoyColorTitle := "Game Boy Color"
	gameCubeTitle := "GameCube"
	genesisTitle := "Genesis"
	nintendo64Title := "Nintendo 64"
	nintendoTitle := "Nintendo"
	nintendoDsTitle := "Nintendo DS"
	playStationTitle := "PlayStation"
	playStation2Title := "PlayStation 2"
	superNintendoTitle := "Super Nintendo"
	wiiTitle := "Wii"

	if strings.Contains(webpage, addHTMLPrefix(dreamcastTitle)) {
		if t.Dreamcast {
			return "dreamcast", nil
		}

		return "", errors.New("dreamcast type excluded from downloading")
	}

	if strings.Contains(webpage, addHTMLPrefix(gameBoyAdvanceTitle)) {
		if t.GameBoyAdvance {
			return "gameboy advance", nil
		}

		return "", errors.New("gameboy advance type excluded from downloading")
	}

	if strings.Contains(webpage, addHTMLPrefix(gameBoyColorTitle)) {
		if t.GameBoyColor {
			return "gameboy color", nil
		}

		return "", errors.New("gameboy color type excluded from downloading")
	}

	if strings.Contains(webpage, addHTMLPrefix(gameBoyTitle)) {
		if t.GameBoy {
			return "gameboy", nil
		}

		return "", errors.New("gameboy type excluded from downloading")
	}

	if strings.Contains(webpage, addHTMLPrefix(gameCubeTitle)) {
		if t.GameCube {
			return "gamecube", nil
		}

		return "", errors.New("gamecube type excluded from downloading")
	}

	if strings.Contains(webpage, addHTMLPrefix(genesisTitle)) {
		if t.Genesis {
			return "genesis", nil
		}

		return "", errors.New("genesis type excluded from downloading")
	}

	if strings.Contains(webpage, addHTMLPrefix(nintendo64Title)) {
		if t.Nintendo64 {
			return "nintendo 64", nil
		}

		return "", errors.New("nintendo 64 type excluded from downloading")
	}

	if strings.Contains(webpage, addHTMLPrefix(nintendoDsTitle)) {
		if t.NintendoDs {
			return "nintendo ds", nil
		}

		return "", errors.New("nintendo ds type excluded from downloading")
	}

	if strings.Contains(webpage, addHTMLPrefix(superNintendoTitle)) {
		if t.SuperNintendo {
			return "super nintendo", nil
		}

		return "", errors.New("super nintendo type excluded from downloading")
	}

	if strings.Contains(webpage, addHTMLPrefix(nintendoTitle)) {
		if t.Nintendo {
			return "nintendo", nil
		}

		return "", errors.New("nintendo type excluded from downloading")
	}

	if strings.Contains(webpage, addHTMLPrefix(playStationTitle)) {
		if t.PlayStation {
			return "playstation", nil
		}

		return "", errors.New("playstation type excluded from downloading")
	}

	if strings.Contains(webpage, addHTMLPrefix(playStation2Title)) {
		if t.PlayStation2 {
			return "playstation 2", nil
		}

		return "", errors.New("playstation 2 type excluded from downloading")
	}

	if strings.Contains(webpage, addHTMLPrefix(wiiTitle)) {
		if t.Wii {
			return "wii", nil
		}

		return "", errors.New("wii type excluded from downloading")
	}

	return "", errors.New("game type could not be determined")
}

// DetermineNumberOfDiscs determines how many discs a game has
func (t *VimmGrabber) DetermineNumberOfDiscs(webpage string) int {
	numberOfDiscs := 0
	discNumber := 1
	containsDownloadDisc := strings.Contains(webpage, fmt.Sprintf("Download"))

	for containsDownloadDisc {
		numberOfDiscs++
		discNumber++

		containsDownloadDisc = strings.Contains(webpage, fmt.Sprintf("Disc %d", discNumber))
	}

	return numberOfDiscs
}
