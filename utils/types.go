package vimmgrabber

// VimmGrabber vimm grabber struct
type VimmGrabber struct {
	Debug           bool
	Dreamcast       bool
	FromGameID      int
	GameBoy         bool
	GameBoyAdvance  bool
	GameBoyColor    bool
	GameCube        bool
	Genesis         bool
	Nintendo        bool
	Nintendo64      bool
	NintendoDs      bool
	PlayStation     bool
	PlayStation2    bool
	SuperNintendo   bool
	ThrottleSeconds int
	ToGameID        int
	Unzip           bool
	Wii             bool
}
