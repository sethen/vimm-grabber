package vimmgrabber

import (
	"net/http"
)

// DownloadGameAtGameID downloads game at the given game id
func (t *VimmGrabber) DownloadGameAtGameID(gameID string) (*http.Response, error) {
	client := &http.Client{}
	req, err := http.NewRequest("POST", "https://download2.vimm.net/download.php?id="+gameID, nil)

	if err != nil {
		return nil, err
	}

	req.Header.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8")
	req.Header.Add("Accept-Encoding", "gzip, deflate, br")
	req.Header.Add("Accept-Language", "en-US,en;q=0.9")
	req.Header.Add("Connection", "keep-alive")
	req.Header.Add("Host", "download.vimm.net")
	req.Header.Add("Referer", "https://vimm.net/vault/?p=details&id="+gameID)
	req.Header.Add("Upgrade-Insecure-Requests", "1")
	req.Header.Add("User-Agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36")

	resp, err := client.Do(req)

	return resp, err
}

// DownloadGameDiscAtGameID downloads the game disc given the game id
func (t *VimmGrabber) DownloadGameDiscAtGameID(gameID string, discNumber string) (*http.Response, error) {
	client := &http.Client{}
	req, err := http.NewRequest("POST", "https://download2.vimm.net/download.php?id="+gameID+"&fileNumber="+discNumber, nil)

	if err != nil {
		return nil, err
	}

	req.Header.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8")
	req.Header.Add("Accept-Encoding", "gzip, deflate, br")
	req.Header.Add("Accept-Language", "en-US,en;q=0.9")
	req.Header.Add("Connection", "keep-alive")
	req.Header.Add("Host", "download.vimm.net")
	req.Header.Add("Referer", "https://vimm.net/vault/?p=details&id="+gameID)
	req.Header.Add("Upgrade-Insecure-Requests", "1")
	req.Header.Add("User-Agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36")

	resp, err := client.Do(req)

	return resp, err
}

// GatherGameDetailsAtGameID gathers game details at the game id
func (t *VimmGrabber) GatherGameDetailsAtGameID(gameID string) (*http.Response, error) {
	resp, err := http.Get("https://vimm.net/vault/?p=details&id=" + gameID)

	return resp, err
}
